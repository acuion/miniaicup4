#include "Bonus.h"
#include "WorldConstants.h"

static inline Point coordToCell(const Point& coord) {
   return Point((coord.mX - WorldConstants::getCellWidth() / 2) / WorldConstants::getCellWidth(),
      (coord.mY - WorldConstants::getCellWidth() / 2) / WorldConstants::getCellWidth());
}

Bonus::Bonus(Type type, const Point& position)
   : mType(type)
   , mPosition(position) {
   mPosition = coordToCell(position);
}

Bonus::Bonus(Type type, int ticksActive)
   : mType(type)
   , mTicksActive(ticksActive) {
}

Bonus::Type Bonus::getType() const {
   return mType;
}

int Bonus::getTicksActive() const {
   return mTicksActive;
}

const Point& Bonus::getPosition() const {
   return mPosition;
}

Bonus::Type Bonus::typeFromString(const std::string& string) {
   if ("saw" == string) {
      return Bonus::Type::SAW;
   }
   else if ("n" == string) {
      return Bonus::Type::NITRO;
   }
   return Bonus::Type::SLOW;
}
