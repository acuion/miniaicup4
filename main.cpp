#include <iostream>
#include "WorldConstants.h"
#include "json.h"
#include "TickInput.h"
#include "Command.h"
#include "Strategy.h"

#ifdef TIMEME
#include <chrono>
#endif

static std::map<std::string, Player> pastPlayersState;

static inline Point coordToCell(const Point& coord) {
   return Point((coord.mX - WorldConstants::getCellWidth() / 2) / WorldConstants::getCellWidth(),
      (coord.mY - WorldConstants::getCellWidth() / 2) / WorldConstants::getCellWidth());
}

int main() {
   std::cin.tie(0);
   std::ios_base::sync_with_stdio(0);

   WorldConstants::read();
   Strategy strategy;

   std::string inputLine;
   while (true) {
      std::getline(std::cin, inputLine);
#ifdef LOC
      // std::clog << inputLine << std::endl;
#endif
      const auto currentInput = nlohmann::json::parse(inputLine);
      if ("end_game" == currentInput["type"]) {
         break;
      }
      const auto tickInfo = currentInput["params"];

      const auto players = tickInfo["players"];
      std::map<std::string, Player> playersMap;
      for (auto it = players.begin(); it != players.end(); ++it) {
         const std::string playerId = it.key();
         const auto player = it.value();

         std::vector<Bonus> bonuses;
         for (auto& bonus : player["bonuses"]) {
            bonuses.emplace_back(Bonus::typeFromString(bonus["type"]), static_cast<int>(bonus["ticks"]));
         }

         std::vector<Point> territory;
         for (auto& point : player["territory"]) {
            territory.emplace_back(point[0], point[1]);
         }

         std::vector<Point> line;
         for (auto& point : player["lines"]) {
            line.emplace_back(point[0], point[1]);
         }

         Point pastPoint(-1, -1);
         if (!pastPlayersState.empty()) {
            pastPoint = pastPlayersState.at(playerId).getPositionCell();
         }

         for (auto& pt : territory) {
            pt = coordToCell(pt);
         }
         for (auto& pt : line) {
            pt = coordToCell(pt);
         }

         playersMap.insert({ playerId, Player(player["score"], territory, coordToCell(Point(player["position"][0], player["position"][1])), line, bonuses, pastPoint) });
      }

      std::vector<Bonus> bonuses;
      for (auto& bonus : tickInfo["bonuses"]) {
         bonuses.emplace_back(Bonus::typeFromString(bonus["type"]), Point(bonus["position"][0], bonus["position"][1]));
      }

      const auto tickInput = TickInput(playersMap, bonuses, tickInfo["tick_num"]);
      Command command;
#ifdef TIMEME
      auto start = std::chrono::system_clock::now();
#endif
      strategy.tick(tickInput, command);
#ifdef TIMEME
      std::clog << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count() << std::endl;
#endif
      command.send();

      pastPlayersState = playersMap;
   }

   return 0;
}
