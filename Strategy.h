#pragma once
#include "TickInput.h"
#include "Command.h"

class Strategy {
public:
   Strategy();

   void tick(const TickInput& tickInput, Command& command);
private:
};

