#pragma once
class WorldConstants {
public:
   WorldConstants() = delete;

   static void read();

   static int getPlayerSpeed();
   static int getCellWidth();
   static int getXCellsCount();
   static int getYCellsCount();
private:
   static int mxCellsCount;
   static int myCellsCount;
   static int mPlayerSpeed;
   static int mCellWidth;
};

