#pragma once

#include <cstddef>

enum MoveDirection : int;

class Point {
public:
   Point();
   Point(int x, int y);

   MoveDirection getMoveDirectionTo(const Point& target) const;

   bool operator<(const Point& second) const;
   Point operator+(const Point& second) const;
   Point operator-(const Point& second) const;
   bool operator==(const Point& second) const;

   int mX;
   int mY;
};

class PointHash {
public:
   size_t operator()(const Point& point) const {
      return (static_cast<size_t>(point.mX) << 32) | point.mY;
   }
};