#pragma once
#include <vector>
#include "Point.h"
#include "Bonus.h"
#include <map>
#include <unordered_set>
#include <string>

class Player {
public:
   Player(int score, const std::vector<Point>& territory, const Point& position, const std::vector<Point>& line,
      const std::vector<Bonus>& activeBonuses, const Point& pastPoint);

   const int getTimeToReach(const Point& target) const;

   int getScore() const;
   const std::vector<Point>& getTerritory() const;
   const std::unordered_set<Point, PointHash>& getTerritorySet() const;
   const Point& getPositionCell() const;
   const std::vector<Point>& getTrail() const;
   const std::vector<Point>& getLineToBase() const;
   const std::unordered_set<Point, PointHash>& getTrailSet() const;
   const std::vector<Bonus>& getActiveBonuses() const;
   const int getSpeed() const;

   const MoveDirection getFirstMoveToBase() const;
   const MoveDirection getFirstMoveToNeutral() const;
   int getTicksToBase() const;

   MoveDirection getFirstMoveToTarget(const Point& target) const;

   const Point& getPastPoint();
   MoveDirection getMoveDirection() const;
   Point getNextPoint() const;
   Point getPrevPoint() const;

   void computeTimeToReach(const std::vector<std::vector<std::string>>& cellBelongsTo, const std::map<std::string, Player>& players);
   std::vector<Point> pathToBaseAfterMove(MoveDirection move) const;
private:
   const int mScore;
   std::vector<Point> mTerritory;
   const Point mPositionCell;
   std::vector<Point> mLine;
   std::vector<Point> mLineToBase;
   const std::vector<Bonus> mActiveBonuses;
   std::unordered_set<Point, PointHash> mTerritorySet;
   std::unordered_set<Point, PointHash> mLineSet;

   std::vector<std::vector<std::pair<Point, int>>> mTimeToReach;
   const std::vector<std::vector<std::string>> mCellBelongsTo;
   int mTimeToGetToBase;
   MoveDirection mFirstMoveToBase;
   int mTimeToGetToNeutral;
   MoveDirection mFirstMoveToNeutral;

   Point mPastPoint;
   MoveDirection mMoveDir;
};
