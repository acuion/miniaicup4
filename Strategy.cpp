#include "Strategy.h"
#include "WorldConstants.h"
#include <algorithm>
#include <cmath>
#include <optional>
#include <queue>
#include <unordered_set>

const int speed = 5;
const int fastSpeed = 6;
const int slowSpeed = 3;

const int trailSizeToRet = 32;
const int caref = 3;

class State {
public:
   State(const Point& _position, MoveDirection _move, int _timePassed, int _speedupTime, int _slowdownTime, int _outsideDfsStep)
      : position(_position)
      , move(_move)
      , timePassed(_timePassed)
      , speedupTime(_speedupTime)
      , slowdownTime(_slowdownTime)
      , outsideDfsStep(_outsideDfsStep) {}

   int getSpeed() const {
      int currentSpeed = speed;
      if (speedupTime > 0 && 0 == slowdownTime) {
         currentSpeed = fastSpeed;
      }
      if (slowdownTime > 0 && 0 == speedupTime) {
         currentSpeed = slowSpeed;
      }
      return currentSpeed;
   }

   Point position;
   MoveDirection move;
   int timePassed;
   int speedupTime;
   int slowdownTime;
   int outsideDfsStep;
};

struct BfsToHomeResult {
   std::unordered_set<Point, PointHash> fullPath;
   std::vector<Point> pathToHome;
   int timeToHome;
};

static const int maxDepth = 8;
static std::vector<State> dfsStack;

static int maxPoints = -1;
static MoveDirection maxMove = UNSET;

Strategy::Strategy() {
}

static bool isOnField(const Point& point) {
   return point.mX >= 0 && point.mY >= 0 && point.mX < WorldConstants::getXCellsCount() && point.mY < WorldConstants::getYCellsCount();
}

static BfsToHomeResult bfsToHome(const TickInput& tickInput, const Player& thisPlayer, const std::unordered_set<Point, PointHash>& pointsWithPlayers, const State& from) {
   // get path to home
   BfsToHomeResult result;
   result.fullPath = thisPlayer.getTrailSet();
   result.timeToHome = 99999;
   if (thisPlayer.getTerritorySet().count(from.position)) {
      result.timeToHome = 0;
   }
   else {
      for (const auto& x : dfsStack) {
         result.fullPath.insert(x.position);
      }

      std::queue<State> q;
      q.push(from);
      std::map<Point, Point> parent;
      parent[from.position] = from.position;
      while (!q.empty()) {
         const auto curr = q.front();
         q.pop();

         if (thisPlayer.getTerritorySet().count(curr.position)) {
            auto pt = curr.position;
            result.timeToHome = curr.timePassed - from.timePassed;
            result.pathToHome.push_back(pt);
            while (!(from.position == parent[pt])) {
               pt = parent[pt];
               result.pathToHome.push_back(pt);
               result.fullPath.insert(pt);
            }
            break;
         }

         for (int mv = MoveDirection::UP; mv <= MoveDirection::LEFT; ++mv) {
            if (reversedMoveDirection[mv] == curr.move) {
               continue;
            }

            const auto nextPoint = curr.position + directionShifts[mv];
            if (!isOnField(nextPoint)
               || result.fullPath.count(nextPoint)
               || pointsWithPlayers.count(nextPoint)
               || parent.count(nextPoint)) {
               continue;
            }

            parent[nextPoint] = curr.position;
            int currentSpeed = speed;
            if (curr.speedupTime > 0 && 0 == curr.slowdownTime) {
               currentSpeed = fastSpeed;
            }
            if (curr.slowdownTime > 0 && 0 == curr.speedupTime) {
               currentSpeed = slowSpeed;
            }
            const int timePerCell = WorldConstants::getCellWidth() / currentSpeed;
            const State nextState{ nextPoint, static_cast<MoveDirection>(mv), curr.timePassed + timePerCell,
               std::max(0, curr.speedupTime - timePerCell),
               std::max(0, curr.slowdownTime - timePerCell),
               curr.outsideDfsStep };
            q.push(nextState);
         }
      }
   }

   return result;
}

static int computePointsForPoint(const TickInput& tickInput, const Point& point) {
   int points = 0;
   const auto ow = tickInput.getCellOwner(point);
   if ("" == ow) {
      points += 1;
   }
   else if ("i" != ow) {
      points += 5;
   }
   for (auto& x : tickInput.getBonuses()) {
      if (Bonus::Type::SAW == x.getType() && point == x.getPosition()) {
         points += 30;
      }
      if (Bonus::Type::NITRO == x.getType() && point == x.getPosition()) {
         points += 15;
      }
      if (Bonus::Type::SLOW == x.getType() && point == x.getPosition()) {
         points -= 20;
      }
   }
   for (auto& x : tickInput.getOtherPlayers()) {
      if (x.second.getTrailSet().count(point)) {
         points += 50;
      }
   }
   return points;
}

static std::vector<std::vector<int>> points(31, std::vector<int>(31, -1));
static std::vector<std::vector<int>> distToEnemyTerr(31, std::vector<int>(31, -1));
static int getDistToEnemyTerr(const Point& pt) {
   return distToEnemyTerr[pt.mX][pt.mY];
}

static void computePointsForPoints(const TickInput& tickInput) {
   std::unordered_set<Point, PointHash> used;
   std::queue<std::pair<Point, int>> q;
   for (auto& pl : tickInput.getOtherPlayers()) {
      for (auto& t : pl.second.getTerritory()) {
         q.emplace(t, 0);
         used.insert(t);
      }
   }

   while (!q.empty()) {
      const auto curr = q.front();
      q.pop();

      points[curr.first.mX][curr.first.mY] = computePointsForPoint(tickInput, curr.first);
      distToEnemyTerr[curr.first.mX][curr.first.mY] = curr.second;

      for (int mv = MoveDirection::UP; mv <= MoveDirection::LEFT; ++mv) {
         const auto nextPoint = curr.first + directionShifts[mv];

         if (!isOnField(nextPoint) || used.count(nextPoint)) {
            continue;
         }

         used.insert(nextPoint);
         q.emplace(nextPoint, curr.second + 1);
      }
   }
}

static int pointsForPoint(const TickInput& tickInput, const Point& point) {
   return points[point.mX][point.mY];
}

static int pointsFromPointWithPath(const TickInput& tickInput, const std::unordered_set<Point, PointHash>& path, const Point& from) {
   int points = 0;

   // get from trail
   for (const auto& x : path) {
      points += pointsForPoint(tickInput, x);
   }

   if (-1 == from.mX || path.size() < 3) {
      return points;
   }

   // get from inside
   std::queue<Point> q;
   q.push(from);
   std::unordered_set<Point, PointHash> used;
   used.insert(from);

   while (!q.empty()) {
      const auto curr = q.front();
      q.pop();

      points += pointsForPoint(tickInput, curr);

      for (int mv = MoveDirection::UP; mv <= MoveDirection::LEFT; ++mv) {
         const auto nextPoint = curr + directionShifts[mv];

         if (path.count(nextPoint) || used.count(nextPoint)) {
            continue;
         }

         used.insert(nextPoint);
         q.push(nextPoint);
      }
   }

   return points;
}

static int pointsFromTrail(const TickInput& tickInput, const Player& thisPlayer, std::vector<Point> orderedPath) {
   if (orderedPath.empty()) {
      return 0;
   }

   const auto start = orderedPath[0];
   const auto end = orderedPath.back();

   std::map<Point, Point> par;
   std::queue<Point> q;
   q.push(start);
   par[start] = start;

   std::unordered_set<Point, PointHash> fullPathSet;
   for (const auto& x : orderedPath) {
      fullPathSet.insert(x);
   }

   while (!q.empty()) {
      const auto curr = q.front();
      q.pop();

      if (end == curr) {
         auto pt = curr;
         while (!(start == par[pt])) {
            pt = par[pt];
            orderedPath.push_back(pt);
            fullPathSet.insert(pt);
         }

         MoveDirection mvDir = orderedPath.back().getMoveDirectionTo(orderedPath[0]);
         std::map<std::pair<MoveDirection, MoveDirection>, int> turns;
         turns[{UP, LEFT}] = 1;
         turns[{LEFT, UP}] = -1;
         turns[{LEFT, DOWN}] = 1;
         turns[{DOWN, LEFT}] = -1;
         turns[{DOWN, RIGHT}] = 1;
         turns[{RIGHT, DOWN}] = -1;
         turns[{RIGHT, UP}] = 1;
         turns[{UP, RIGHT}] = -1;
         int leftMinusRight = 0;
         Point leftPt(-1, -1), rightPt(-1, -1);
         for (int i = 1; i < orderedPath.size(); ++i) {
            MoveDirection currMv = orderedPath[i - 1].getMoveDirectionTo(orderedPath[i]);
            if (UP == currMv) {
               const auto p1 = orderedPath[i] + directionShifts[LEFT];
               const auto p2 = orderedPath[i] + directionShifts[RIGHT];

               if (isOnField(p1) && !fullPathSet.count(p1)) {
                  leftPt = p1;
               }
               if (isOnField(p2) && !fullPathSet.count(p2)) {
                  rightPt = p2;
               }
            }
            leftMinusRight += turns[{mvDir, currMv}];
            mvDir = currMv;
         }

         if (leftMinusRight > 0) {
            return pointsFromPointWithPath(tickInput, fullPathSet, leftPt);
         }
         else {
            return pointsFromPointWithPath(tickInput, fullPathSet, rightPt);
         }

         break;
      }

      for (int mv = MoveDirection::UP; mv <= MoveDirection::LEFT; ++mv) {
         const auto nextPoint = curr + directionShifts[mv];

         if (!isOnField(nextPoint)
            || par.count(nextPoint)
            || (fullPathSet.count(nextPoint) && !(nextPoint == end))
            || (!thisPlayer.getTerritorySet().count(nextPoint) && !(nextPoint == end))) {
            continue;
         }

         par[nextPoint] = curr;
         q.push(nextPoint);
      }
   }

   return 0;
}

static void dfs(const TickInput& tickInput, const Player& thisPlayer, const std::unordered_set<Point, PointHash>& pointsWithPlayers) {
   if (dfsStack.size() > maxDepth) {
      dfsStack.pop_back();
      return;
   }

   const State currState = dfsStack.back();
   const Point currPoint = currState.position;

   {
      const auto homeData = bfsToHome(tickInput, thisPlayer, pointsWithPlayers, currState);

      // analyze the full path
      int timeToTrail = 99999;
      for (const auto& pl : tickInput.getOtherPlayers()) {
         if (UNSET == pl.second.getMoveDirection()) {
            continue;
         }
         for (const auto& pt : homeData.fullPath) {
            if (thisPlayer.getTerritorySet().count(pt)) {
               continue;
            }
            const auto time = pl.second.getTimeToReach(pt);
            if (-1 == time) {
               continue;
            }
            timeToTrail = std::min(timeToTrail, time);
         }
      }

      if (timeToTrail - caref * WorldConstants::getCellWidth() / currState.getSpeed() <= currState.timePassed + homeData.timeToHome
         || homeData.fullPath.size() > trailSizeToRet) {
         dfsStack.pop_back(); // dangerous
         return;
      }

      if (dfsStack.size() > 1 && -1 != currState.outsideDfsStep) {
         // compute points
         std::vector<Point> orderedPath = thisPlayer.getTrail();
         if (orderedPath.size()) {
            orderedPath.pop_back(); // curr position
         }
         for (int i = currState.outsideDfsStep; i < dfsStack.size(); ++i) {
            orderedPath.push_back(dfsStack[i].position);
         }
         for (int i = static_cast<int>(homeData.pathToHome.size()) - 1; i > 0; --i) {
            orderedPath.push_back(homeData.pathToHome[i]);
         }
         int pointsFromEnemyDist = 0;
         for (auto& pt : orderedPath) {
            pointsFromEnemyDist = std::max(15 - getDistToEnemyTerr(pt) / 2, pointsFromEnemyDist);
         }
         const int points = pointsFromTrail(tickInput, thisPlayer, orderedPath) + pointsFromEnemyDist;

         if (maxPoints < points) {
            maxPoints = points;
            maxMove = dfsStack[0].position.getMoveDirectionTo(dfsStack[1].position);
         }
      }
   }

   for (int mv = currState.move == UNSET ? UP : currState.move, i = 0; i < 4; ++i, mv = (mv + 1) % MoveDirection::UNSET) {
      if (reversedMoveDirection[mv] == currState.move) {
         continue;
      }

      const auto move = static_cast<MoveDirection>(mv);

      // basic checks
      const auto nextPoint = currPoint + directionShifts[mv];
      if (!isOnField(nextPoint)
         || thisPlayer.getTrailSet().count(nextPoint)
         || pointsWithPlayers.count(nextPoint)
         || (-1 != currState.outsideDfsStep && thisPlayer.getTerritorySet().count(nextPoint))) {
         continue;
      }

      if (!thisPlayer.getTerritorySet().count(nextPoint)) {
         bool stepOnTrail = false;
         for (const auto& st : dfsStack) {
            if (st.position == nextPoint) {
               stepOnTrail = true;
               break;
            }
         }
         if (stepOnTrail) {
            continue;
         }
      }

      const int timePerCell = WorldConstants::getCellWidth() / currState.getSpeed();

      auto outsideStep = currState.outsideDfsStep;
      if (-1 == outsideStep && !thisPlayer.getTerritorySet().count(nextPoint)) {
         outsideStep = static_cast<int>(dfsStack.size());
      }
      dfsStack.emplace_back(nextPoint, move, currState.timePassed + timePerCell,
         std::max(0, currState.speedupTime - timePerCell),
         std::max(0, currState.slowdownTime - timePerCell),
         outsideStep);
      dfs(tickInput, thisPlayer, pointsWithPlayers);
   }

   dfsStack.pop_back();
}

static MoveDirection safeMoveWaste(const Player& thisPlayer, const TickInput& tickInput) {
   for (int dir = MoveDirection::UP; dir <= MoveDirection::LEFT; ++dir) {
      const auto nextPoint = thisPlayer.getPositionCell() + directionShifts[dir];
      if (thisPlayer.getPrevPoint() == nextPoint) {
         continue;
      }
      if (thisPlayer.getTerritorySet().count(nextPoint)) {
         return static_cast<MoveDirection>(dir);
      }
   }

   return thisPlayer.getFirstMoveToTarget(thisPlayer.getPrevPoint()); // almost random
}


void Strategy::tick(const TickInput& tickInput, Command& command) {
   const auto& thisPlayer = tickInput.getThisPlayer();
   if (!thisPlayer.getTerritorySet().count(thisPlayer.getPositionCell()) && thisPlayer.getTerritorySet().count(thisPlayer.getNextPoint())) {
      command.go(thisPlayer.getMoveDirection());
      return;
   }
   //
   std::unordered_set<Point, PointHash> pointsWithPlayers;
   for (const auto& p : tickInput.getOtherPlayers()) {
      if (p.second.getTrail().size() <= thisPlayer.getTrail().size()) {
         pointsWithPlayers.insert(p.second.getPositionCell());
      }
   }

   {
      int fasttime = 0;
      int slowtime = 0;
      for (const auto& bonus : thisPlayer.getActiveBonuses()) {
         if (Bonus::Type::NITRO == bonus.getType()) {
            fasttime = bonus.getTicksActive();
         }
         if (Bonus::Type::SLOW == bonus.getType()) {
            slowtime = bonus.getTicksActive();
         }
      }

      dfsStack.emplace_back(thisPlayer.getPositionCell(), thisPlayer.getMoveDirection(), 0,
         fasttime, slowtime, thisPlayer.getTrail().size() ? 0 : -1);
   }

   maxPoints = -1;
   maxMove = UNSET;
   computePointsForPoints(tickInput);
   dfs(tickInput, thisPlayer, pointsWithPlayers);
   dfsStack.clear();

   if (UNSET == maxMove) {
      if (!thisPlayer.getTerritorySet().count(thisPlayer.getPositionCell())) {
         command.go(thisPlayer.getFirstMoveToBase());
      }
      else {
         if (thisPlayer.getTerritorySet().count(thisPlayer.getPositionCell()
            + directionShifts[static_cast<int>(thisPlayer.getFirstMoveToNeutral())])) {
            command.go(thisPlayer.getFirstMoveToNeutral());
         }
         else {
            command.go(safeMoveWaste(thisPlayer, tickInput));
         }
      }
   }
   else {
      command.go(maxMove);
   }
}
