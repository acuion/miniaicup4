#include "Point.h"
#include "Movement.h"

Point::Point()
   : Point(0, 0) {
}

Point::Point(int x, int y)
   : mX(x)
   , mY(y) {
}

MoveDirection Point::getMoveDirectionTo(const Point& target) const {
   if (mX < target.mX) {
      return MoveDirection::RIGHT;
   }
   if (mX > target.mX) {
      return MoveDirection::LEFT;
   }
   if (mY < target.mY) {
      return MoveDirection::UP;
   }
   if (mY > target.mY) {
      return MoveDirection::DOWN;
   }
   return MoveDirection::UNSET;
}

bool Point::operator<(const Point& second) const {
	if (mX != second.mX) {
		return mX < second.mX;
	}
	return mY < second.mY;
}

Point Point::operator+(const Point& second) const {
   return Point(mX + second.mX, mY + second.mY);
}

Point Point::operator-(const Point& second) const {
   return Point(mX - second.mX, mY - second.mY);
}

bool Point::operator==(const Point& second) const {
   return mX == second.mX && mY == second.mY;
}
