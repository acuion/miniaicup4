#include "Player.h"
#include "WorldConstants.h"
#include "Movement.h"
#include <queue>
#include <iostream>
#include <algorithm>

struct State {
   MoveDirection mvDir;
   Point pos;
   MoveDirection firstMove;

   int mNeighbours = 0;

   void countNeighbours(const std::unordered_set<Point, PointHash>& territory, const std::unordered_set<Point, PointHash>& trail) {
      for (int dir = MoveDirection::UP; dir <= MoveDirection::LEFT; ++dir) {
         const auto newPoint = pos + directionShifts[dir];
         mNeighbours += static_cast<int>(territory.count(newPoint)) + static_cast<int>(trail.count(newPoint));
      }
   }

   bool operator <(const State& state) const {
      return mNeighbours < state.mNeighbours;
   };
};

Player::Player(int score, const std::vector<Point>& territory, const Point& position, const std::vector<Point>& line,
   const std::vector<Bonus>& activeBonuses, const Point& pastPoint)
   : mScore(score)
   , mTerritory(territory)
   , mLine(line)
   , mPositionCell(position)
   , mActiveBonuses(activeBonuses)
   , mPastPoint(pastPoint)
   , mMoveDir(MoveDirection::UNSET) {
   for (auto& pt : mTerritory) {
      mTerritorySet.insert(pt);
   }
   for (auto& pt : mLine) {
      mLineSet.insert(pt);
   }

   if (-1 != getPastPoint().mX) {
      mMoveDir = getPastPoint().getMoveDirectionTo(getPositionCell());
   }
}

static bool isOnField(const Point& point) {
   return point.mX >= 0 && point.mY >= 0 && point.mX < WorldConstants::getXCellsCount() && point.mY < WorldConstants::getYCellsCount();
}

void Player::computeTimeToReach(const std::vector<std::vector<std::string>>& cellBelongsTo, const std::map<std::string, Player>& players) {
   std::unordered_set<Point, PointHash> pointsWithPlayers;
   for (auto& p : players) {
      if (p.second.getTrail().size() <= getTrail().size()) {
         pointsWithPlayers.insert(p.second.getPositionCell());
      }
   }

   mTimeToReach.resize(WorldConstants::getXCellsCount(), std::vector <std::pair<Point, int>> (WorldConstants::getYCellsCount(), { Point(-1, -1), -1 }));
   mTimeToReach[getPositionCell().mX][getPositionCell().mY] = { Point(-1, -1), 0 };

   std::queue<State> q;
   q.push({ mMoveDir, getPositionCell(), MoveDirection::UNSET });

   const int ticksPerCell = WorldConstants::getCellWidth() / getSpeed(); // todo: changing over time
   mTimeToGetToBase = -1;
   mTimeToGetToNeutral = -1;

   std::deque<State> order;
   while (!q.empty()) {
      const auto thisState = q.front();
      q.pop();
      order.clear();

      if (-1 == mTimeToGetToBase && getTerritorySet().count(thisState.pos)) {
         mTimeToGetToBase = getTimeToReach(thisState.pos);
         mFirstMoveToBase = thisState.firstMove;

         auto start = thisState.pos;
         while (-1 != mTimeToReach[start.mX][start.mY].first.mX) {
            start = mTimeToReach[start.mX][start.mY].first;
            mLineToBase.push_back(start);
         }

         if (mLineToBase.size()) {
            mLineToBase.pop_back();
         }
      }

      if (-1 == mTimeToGetToNeutral && "" == cellBelongsTo[thisState.pos.mX][thisState.pos.mY]) {
         mTimeToGetToNeutral = getTimeToReach(thisState.pos);
         mFirstMoveToNeutral = thisState.firstMove;
      }

      for (int dir = MoveDirection::UP; dir <= MoveDirection::LEFT; ++dir) {
         if (reversedMoveDirection[dir] == thisState.mvDir) {
            continue;
         }

         const auto newPoint = thisState.pos + directionShifts[dir];
         if (!isOnField(newPoint) || -1 != mTimeToReach[newPoint.mX][newPoint.mY].second || getTrailSet().count(newPoint) || pointsWithPlayers.count(newPoint)) {
            continue;
         }

         mTimeToReach[newPoint.mX][newPoint.mY] = { thisState.pos, getTimeToReach(thisState.pos) + ticksPerCell };
         State newState = { static_cast<MoveDirection>(dir), newPoint,
            thisState.firstMove == MoveDirection::UNSET ? static_cast<MoveDirection>(dir) : thisState.firstMove };
         newState.countNeighbours(getTerritorySet(), getTrailSet());
         order.push_back(newState);
      }

      for (auto it = order.begin(); it != order.end(); ++it) {
         if (mMoveDir == it->mvDir) {
            const auto stateToFront = *it;
            order.erase(it);
            order.push_front(stateToFront);
            break;
         }
      }
      stable_sort(order.begin(), order.end());

      for (auto& x : order) {
         q.push(x);
      }
   }
}

std::vector<Point> Player::pathToBaseAfterMove(MoveDirection move) const {
   return std::vector<Point>();
}

const int Player::getTimeToReach(const Point& target) const {
   if (!isOnField(target)) {
      return -1;
   }
   return mTimeToReach[target.mX][target.mY].second;
}

int Player::getScore() const {
   return mScore;
}

const std::vector<Point>& Player::getTerritory() const {
   return mTerritory;
}

const std::unordered_set<Point, PointHash>& Player::getTerritorySet() const {
   return mTerritorySet;
}

const Point& Player::getPositionCell() const {
   return mPositionCell;
}

const std::vector<Point>& Player::getTrail() const {
   return mLine;
}

const std::vector<Point>& Player::getLineToBase() const {
   return mLineToBase;
}

const std::unordered_set<Point, PointHash>& Player::getTrailSet() const {
   return mLineSet;
}

const std::vector<Bonus>& Player::getActiveBonuses() const {
   return mActiveBonuses;
}

const int Player::getSpeed() const {
   int speed = WorldConstants::getPlayerSpeed();
   int incdec;

   {
      bool speedBonus, slowBonus;
      speedBonus = slowBonus = false;
      for (auto& b : mActiveBonuses) {
         speedBonus |= Bonus::Type::NITRO == b.getType();
         slowBonus |= Bonus::Type::SLOW == b.getType();
      }
      incdec = speedBonus - slowBonus;
   }

   if (0 != incdec) {
      while (speed < WorldConstants::getCellWidth()) {
         speed += incdec;
         if (0 == WorldConstants::getCellWidth() % speed) {
            break;
         }
      }
   }

   return speed;
}

const MoveDirection Player::getFirstMoveToBase() const {
   return mFirstMoveToBase;
}

const MoveDirection Player::getFirstMoveToNeutral() const {
   return mFirstMoveToNeutral;
}

int Player::getTicksToBase() const {
   return mTimeToGetToBase;
}

MoveDirection Player::getFirstMoveToTarget(const Point& target) const {
   auto start = mTimeToReach[target.mX][target.mY];
   std::pair<Point, int> prv = { target, -1 };
   if (-1 == start.second || -1 == start.first.mX) {
      return MoveDirection::UNSET;
   }

   while (0 != mTimeToReach[start.first.mX][start.first.mY].second) {
      prv = start;
      start = mTimeToReach[start.first.mX][start.first.mY];
   }

   return getPositionCell().getMoveDirectionTo(prv.first);
}

const Point& Player::getPastPoint() {
   return mPastPoint;
}

MoveDirection Player::getMoveDirection() const {
   return mMoveDir;
}

Point Player::getNextPoint() const {
   return getPositionCell() + directionShifts[mMoveDir];
}

Point Player::getPrevPoint() const {
   return getPositionCell() - directionShifts[mMoveDir];
}
