#pragma once
#include "Player.h"
#include "Bonus.h"
#include "Point.h"
#include <map>
#include <string>
#include <vector>

class TickInput {
public:
   TickInput(const std::map<std::string, Player>& players, const std::vector<Bonus>& bonuses, int tickNumber);

   const Player& getThisPlayer() const;
   const std::map<std::string, Player> getOtherPlayers() const;
   const std::vector<Bonus>& getBonuses() const;
   const int getTickNumber() const;
   const std::string& getCellOwner(const Point& cell) const;
   const std::vector<std::vector<std::string>>& getCellOwners() const;
private:
   Player mThisPlayer;
   std::map<std::string, Player> mPlayers;
   const std::vector<Bonus> mBonuses;
   const int mTickNumber;
   std::vector<std::vector<std::string>> mCellBelongsTo;
};
