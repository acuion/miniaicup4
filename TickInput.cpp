#include "TickInput.h"
#include "WorldConstants.h"

TickInput::TickInput(const std::map<std::string, Player>& players, const std::vector<Bonus>& bonuses, int tickNumber)
   : mThisPlayer(players.at("i"))
   , mPlayers(players)
   , mBonuses(bonuses)
   , mTickNumber(tickNumber) {
   mCellBelongsTo.resize(WorldConstants::getXCellsCount(), std::vector<std::string>(WorldConstants::getYCellsCount(), ""));

   for (auto& pl : mPlayers) {
      for (auto& tCell : pl.second.getTerritory()) {
         mCellBelongsTo[tCell.mX][tCell.mY] = pl.first;
      }
      //for (auto& tCell : pl.second.getTrail()) {
      //   mCellBelongsTo[tCell.mX][tCell.mY] = pl.first; // ?
      //}
   }

   mPlayers.erase("i");
   const auto emptyMap = std::map<std::string, Player>();
   for (auto& x : mPlayers) {
      x.second.computeTimeToReach(mCellBelongsTo, emptyMap);
   }
   mThisPlayer.computeTimeToReach(mCellBelongsTo, mPlayers);
}

const Player& TickInput::getThisPlayer() const {
   return mThisPlayer;
}

const std::map<std::string, Player> TickInput::getOtherPlayers() const {
   return mPlayers;
}

const std::vector<Bonus>& TickInput::getBonuses() const {
   return mBonuses;
}

const int TickInput::getTickNumber() const {
   return mTickNumber;
}

const std::string& TickInput::getCellOwner(const Point& cell) const {
   return mCellBelongsTo[cell.mX][cell.mY];
}

const std::vector<std::vector<std::string>>& TickInput::getCellOwners() const {
   return mCellBelongsTo;
}
