#pragma once
#include <string>
#include <iostream>
#include "Movement.h"

class Command {
public:
   Command();

   void go(MoveDirection direction);
   void send();
private:
   MoveDirection mSelectedDirection;
};
