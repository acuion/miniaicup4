#include "WorldConstants.h"
#include "json.h"
#include <iostream>
#include <string>

int WorldConstants::mxCellsCount;
int WorldConstants::myCellsCount;
int WorldConstants::mPlayerSpeed;
int WorldConstants::mCellWidth;

void WorldConstants::read() {
   std::string inputLine;
   std::getline(std::cin, inputLine);
   const auto constantsJson = nlohmann::json::parse(inputLine)["params"];
   mxCellsCount = constantsJson["x_cells_count"];
   myCellsCount = constantsJson["y_cells_count"];
   mPlayerSpeed = constantsJson["speed"];
   mCellWidth = constantsJson["width"];
}

int WorldConstants::getXCellsCount() {
   return mxCellsCount;
}

int WorldConstants::getYCellsCount() {
   return myCellsCount;
}

int WorldConstants::getPlayerSpeed() {
   return mPlayerSpeed;
}

int WorldConstants::getCellWidth() {
   return mCellWidth;
}
