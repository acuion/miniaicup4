#pragma once
#include "Point.h"
#include <string>

class Bonus {
public:
   enum class Type {
      NITRO,
      SLOW,
      SAW
   };

   Bonus(Type type, const Point& position);
   Bonus(Type type, int ticksActive);

   Type getType() const;
   int getTicksActive() const;
   const Point& getPosition() const;

   static Type typeFromString(const std::string& string);
private:
   Type mType;
   int mTicksActive;
   Point mPosition;
};
