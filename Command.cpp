#include "Command.h"

Command::Command() 
   : mSelectedDirection(MoveDirection::UNSET) {
}

void Command::go(MoveDirection direction) {
   mSelectedDirection = direction;
}

void Command::send() {
   switch (mSelectedDirection) {
   case MoveDirection::UP:
      std::cout << "{\"command\": \"up\"}" << std::endl;
      break;
   case MoveDirection::RIGHT:
      std::cout << "{\"command\": \"right\"}" << std::endl;
      break;
   case MoveDirection::DOWN:
      std::cout << "{\"command\": \"down\"}" << std::endl;
      break;
   case MoveDirection::LEFT:
      std::cout << "{\"command\": \"left\"}" << std::endl;
      break;
   default:
      throw 0;
   }
}
