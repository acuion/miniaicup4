#pragma once
#include "Point.h"
#include <vector>

enum MoveDirection : int {
   UP = 0,
   RIGHT = 1,
   DOWN = 2,
   LEFT = 3,
   UNSET = 4
};

static std::vector<MoveDirection> reversedMoveDirection = { DOWN, LEFT, UP, RIGHT };
static std::vector<Point> directionShifts = { Point(0, 1), Point(1, 0), Point(0, -1), Point(-1, 0) };
